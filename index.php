<?php
?>

<html>
    <head>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="productreviewwidget" data-itemid="299164" data-full-width="1" data-num-reviews="3" data-order="best" data-css-override='true' data-custom-css='https://dl.dropbox.com/s/oth1747lhmqj1l4/app.css?dl=0'></div><script type="text/javascript">
                    (function() {
                        function async_load(){
                            var s = document.createElement("script");
                            s.type = "text/javascript";
                            s.async = true;
                            s.src = "//www.productreview.com.au/assets/js/widget/reviews-itemid.js";
                            var x = document.getElementsByTagName("script")[0];
                            x.parentNode.insertBefore(s, x);
                        }
                        if (window.attachEvent) {w
                            window.attachEvent("onload", async_load);
                        } else {
                            window.addEventListener("load", async_load, false);
                        }
                    })();
                    </script>
                </div>
            </div>
        </div>
    </body>
</html>